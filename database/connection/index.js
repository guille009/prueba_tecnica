let mongoose = require('mongoose');
let utils = require('./utils');

mongoose.set('useFindAndModify', false);
mongoose.set('useNewUrlParser', true);
mongoose.set('useCreateIndex', true);

mongoose.connect(utils.mongodb_uri)
.then(()=>{
    console.log("Exito al realizar la conexion con mongoDB");
})
.catch((error)=>{
    console.log("Error al realizar la conexion con mongoDB");
});

module.exports = mongoose;