let mongoose = require("mongoose");
let Schema = mongoose.Schema;
let Comments = require('./comments');

let CoffeeShopsSchema = new Schema({
    name: {type: String},
    is_comentable: {type: Boolean, default: true},
    comments: [Comments],

});

module.exports = mongoose.model('CoffeeShops', CoffeeShopsSchema);