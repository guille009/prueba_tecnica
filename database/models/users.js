let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let Users = new Schema({
    user: {type: String, required: true, index: { unique: true }},
    password: {type: String, required: true},
    is_admin: {type: Boolean, default: false}
});

module.exports = mongoose.model('Users', Users);