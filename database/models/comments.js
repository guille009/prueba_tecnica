let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let Comments = new Schema({
    id_user: {type: Schema.Types.ObjectId, ref: 'Users'},
    comment: {type: String}
});

module.exports = Comments;