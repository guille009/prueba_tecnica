let mongoose = require('mongoose');
let utils = require('../connection/utils');
let InitDataBaseService = require('../../services/initDataBase/initDataBaseService');
let initDataBaseService = new InitDataBaseService();

mongoose.connect(utils.mongodb_uri).
then(()=>{
    return initDataBaseService.deleteBBDD();
}).
then(()=>{
    return initDataBaseService.initialData();
}).
then(()=>{
    console.log("Finished. the database has been initialized");
})
.catch((error)=>{
    console.log(error);
});