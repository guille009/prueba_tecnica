let UserService = require('../userService');
let CoffeShopService = require('../coffeShopService');
let userService = new UserService();
let coffeShopService = new CoffeShopService();
let mongoose = require('mongoose');

class InitDataBaseService{

    async deleteBBDD(){
        return await mongoose.connection.db.dropDatabase();
    }

    async initialData(){
        return await Promise.all([
            userService.createNewUser(InitDataBaseService.prototype.adminUserSample),
            userService.createNewUser(InitDataBaseService.prototype.noAdminUserSample),
            coffeShopService.createCoffeShops(InitDataBaseService.prototype.CoffeeShopsWithCommentsSample),
            coffeShopService.createCoffeShops(InitDataBaseService.prototype.CoffeeShopsWithoutCommentsSample)
        ]);
    }

    async getNormalUser(){
        return await userService.getByUserName("usuario");
    }

    async getAdminUser(){
        return await userService.getByUserName("usuario_admin");
    }

    async getCommentableCoffeeShop(){
        return await coffeShopService.getByName("cafeteria_cometable");
    }

    async getUnommentableCoffeeShop(){
        return await coffeShopService.getByName("cafeteria_no_comentable");
    }
}

InitDataBaseService.prototype.adminUserSample = {
    user: "usuario_admin",
    password: "contra",
    is_admin: true
};

InitDataBaseService.prototype.noAdminUserSample = {
    user: "usuario",
    password: "contra",
    is_admin: false
};

InitDataBaseService.prototype.CoffeeShopsWithCommentsSample = {
    name: "cafeteria_cometable",
    comments: [],
    is_comentable: true
};

InitDataBaseService.prototype.CoffeeShopsWithoutCommentsSample = {
    name: "cafeteria_no_comentable",
    comments: [],
    is_comentable: false
};

module.exports = InitDataBaseService;