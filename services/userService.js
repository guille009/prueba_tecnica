let Users = require('../database/models/users');
let sha256 = require("sha256");

class UserService{

    async createNewUser(data){
        let user = await Users.findOne({user: data.user});
        if(user)
            return Promise.reject({msg: "The user not exist", error_code: 400});
        return await this.createUser(data);
    }

    async createUser(data){
        return await Users.create({
            user: data.user,
            password: sha256(data.password),
            is_admin: data.is_admin
        });
    }

    async checkLogin(data){
        let user = await Users.findOne({"user": data.user});
        if(!user || (user.password != sha256(data.password)))
            return Promise.reject({msg: "User or password incorrect", error_code: 400});
        return Promise.resolve(user);
    }

    async getByUserName(user){ // used in test
        return await Users.findOne({user: user});
    }

    async getById(id){
        return await Users.findById(id);
    }
}

module.exports = UserService;