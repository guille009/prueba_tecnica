let CoffeeShops = require('../database/models/coffee_shops');
let UserService = require('./userService');
let userService = new UserService();

class CoffeeShopsService{

    async getAllWithoutComments(){
        return await this.getAll("-comments");
    }

    async getAll(fields_filter){
        return await CoffeeShops.find({}, fields_filter);
    }

    async getById(id){
        return await CoffeeShops.findById(id);
    }

    async createCoffeShops(data){
        return await CoffeeShops.create(data);
    }

    async getCommentsByCoffeeShop(id_coffee_shop){
        return await CoffeeShops.findById(id_coffee_shop, "comments");
    }

    async addNewComment(id_coffee_shop, id_user, data_comment){
        let coffeee_shop = await this.getById(id_coffee_shop);
        if(!coffeee_shop.is_comentable){
            let user = await userService.getById(id_user);
            if(!user.is_admin)
                return Promise.reject({msg: "The coffee shop is uncomentable", error_code: 401});
        }
        data_comment.id_user = id_user;
        coffeee_shop.comments.push(data_comment);
        return await coffeee_shop.save();
    }

    getLastCommentByCoffeeShop(coffee_shop){
        if(coffee_shop.comments.length < 1)
            return null;
        return coffee_shop.comments[coffee_shop.comments.length-1]
    }

    async getByName(name){
        return await CoffeeShops.findOne({name: name});
    }
}

module.exports = CoffeeShopsService;