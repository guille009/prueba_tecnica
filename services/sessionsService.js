class SessionService{

    isSessionInit(req, fields_check){
        if(!req.session)
            return false;
        for(let field_check of fields_check){
            if(!req.session[field_check])
                return false;
        }
        return true;
    }

    createSession(req, data){
        Object.assign(req.session, data);
    }

    destroySession(req){
        req.session.destroy();
    }

    getSessionData(req){
        return req.session;
    }

}

module.exports = SessionService;