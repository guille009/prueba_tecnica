let SessionService = require('../../services/sessionsService');
let sessionService = new SessionService();

let login_page = "/";
let home_page = "/coffeeshops";

exports.redirectUserNotLogged = (req, res, next) => {
    if(!sessionService.isSessionInit(req, ["id_user"]) )
        return res.redirect(login_page);
    next();
};

exports.redirectUserLogged = (req, res, next) => {
    if(sessionService.isSessionInit(req, ["id_user"]) )
        return res.redirect(home_page);
    next();
};