var express = require('express');
var router = express.Router();
let auth_control = require('../auth_control/');

/* GET home page. */
router.get('/', auth_control.redirectUserLogged, function(req, res, next) {
    res.render('login');
});

router.get('/coffeeshops', auth_control.redirectUserNotLogged, function(req, res, next) {
    res.render('coffeeshops');
});


module.exports = router;

