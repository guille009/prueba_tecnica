$( document ).ready(function() {
    let ul_list_coffee_shops = $("#coffee_shops_list");
    let name_coffe_shop = $("#name_coffe_shop");
    let is_comentable = $("#is_comentable");
    let send_coffee_shop = $("#send_coffee_shop");
    let ul_coffee_shop_comments = $("#coffee_shops_comments");
    let comment_coffe_shop = $("#comment_coffe_shop");
    let send_comment = $("#send_comment");
    let comment_col = $("#comment_col");

    comment_col.hide();

    $.ajax({
        type: "GET",
        url: "/api/1.0/coffeeshops/",
        dataType: "json",
        success: (coffee_shops) => {
            addCoffeeShopsElement(coffee_shops);
        },
        error: (xhr, status, er) => {
            let msg_error = xhr.responseJSON.msg ? xhr.responseJSON.msg: "An error was happend";
            alert(msg_error);
        }
    });

    function addCoffeeShopsElement(coffee_shops){
        for(let coffee_shop of coffee_shops){
            addLiAElementToUl(ul_list_coffee_shops, coffee_shop._id, coffee_shop.name);
        }
    }

    function addLiAElementToUl(ul_element, id, name){
        ul_element.append("<li><button type='button' value="+id+" class='coffee_shops btn btn-link'>"+name+"</button></li>");
    }

    send_coffee_shop.click((ev)=>{
        let data = {
            name: name_coffe_shop.val(),
            is_comentable: is_comentable.val() == "YES",
        };
        $.ajax({
            type: "POST",
            url: "/api/1.0/coffeeshops/",
            data: data,
            dataType: "json",
            success: (coffee_shop) => {
                addLiAElementToUl(ul_list_coffee_shops, coffee_shop._id, coffee_shop.name)
            },
            error: (xhr, status, er) => {
                let msg_error = xhr.responseJSON.msg ? xhr.responseJSON.msg: "An error was happend";
                alert(msg_error);
            }
        });
    });

    $("ul#coffee_shops_list").on("click", "button", function(ev){
        let coffe_shop = $(this);
        let id = coffe_shop.val();
        send_comment.val(id);
        $.ajax({
            type: "GET",
            url: "/api/1.0/coffeeshops/"+id+"/comments",
            dataType: "json",
            success: (data) => {
                comment_col.show();
                addComments(data.comments);
            },
            error: (xhr, status, er) => {
                let msg_error = xhr.responseJSON.msg ? xhr.responseJSON.msg: "An error was happend";
                alert(msg_error);
            }
        });
    });

    function addComments(comments){
        ul_coffee_shop_comments.text("");
        for(let comment of comments){
            addLiAElementToUl(ul_coffee_shop_comments, comment._id, comment.comment);
        }
    }

    send_comment.click(function (ev) {
        let data = {
            comment: comment_coffe_shop.val()
        };
        $.ajax({
            type: "POST",
            url: "/api/1.0/coffeeshops/"+($(this).val())+"/comments",
            data: data,
            dataType: "json",
            success: (comment) => {
                addLiAElementToUl(ul_coffee_shop_comments, comment._id, comment.comment)
            },
            error: (xhr, status, er) => {
                let msg_error = xhr.responseJSON.msg ? xhr.responseJSON.msg: "An error was happend";
                alert(msg_error);
            }
        });
    });

});