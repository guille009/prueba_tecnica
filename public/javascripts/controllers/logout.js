$( document ).ready(function() {

    let log_out = $("#log_out");

    log_out.click((ev)=>{
        $.ajax({
            type: "POST",
            url: "/api/1.0/users/logout",
            dataType: "json",
            success: (data, statusCode, xhr) => {
                if(xhr.status == 200)
                    window.location.href = "/";
                else
                    alert(data.msg);
            },
            error: () => {
                console.log("error");
            }
        });
    })
});