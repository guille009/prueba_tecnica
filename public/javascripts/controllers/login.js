$( document ).ready(function() {

    let user_login = $("#user_login");
    let password_login = $("#password_login");
    let enviar_login = $("#enviar_login");

    enviar_login.click((ev)=>{
        let data = {
            user: user_login.val(),
            password: password_login.val()
        };
        $.ajax({
            type: "POST",
            url: "/api/1.0/users/login",
            data: data,
            dataType: "json",
            success: (data, statusCode, xhr) => {
                if(xhr.status == 200)
                    window.location.href = "/coffeeshops";
                else
                    alert(data.msg);
            },
            error: () => {
                console.log("error");
            }
        });
    })
});