$( document ).ready(function() {
    let user_registro = $("#user_registro");
    let password_registro = $("#password_registro");
    let es_admin_registro = $("#es_admin_registro");
    let enviar_registro = $("#enviar_registro");

    enviar_registro.click((ev)=>{
        let data = {
            user: user_registro.val(),
            password: password_registro.val(),
            is_admin: es_admin_registro.val() == "YES",
        };
        $.ajax({
            type: "POST",
            url: "/api/1.0/users/",
            data: data,
            dataType: "json",
            success: () => {
                console.log("exito");
            },
            error: () => {
                console.log("error");
            }
        });
    })
});