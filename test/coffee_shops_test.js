let assert = require('chai').assert;
let utils = require('./utils');
let mongoose = require('mongoose');
let InitDataBaseService = require('../services/initDataBase/initDataBaseService');
let CoffeeShopService = require('../services/coffeShopService');
let initDataBaseService = new InitDataBaseService();
let coffeeShopService = new CoffeeShopService();

describe("Coffee shops test", function() {

    before(async() => {
        if (mongoose.connection.db) return Promise.resolve();
        return await mongoose.connect(utils.db_uri_test);
    });

    before(async() => {
        return await initDataBaseService.deleteBBDD();
    });

    before(async() => {
        await initDataBaseService.initialData();
    });

    it("List of coffee shops", async()=>{
        let coffee_shops = await coffeeShopService.getAllWithoutComments();
        assert.equal(coffee_shops.length>0, true, "Error, in the list of coffee_shops");
        return Promise.resolve();
    });

    it("Create coffee_shop comentable", async()=>{
        let coffee_shop = await coffeeShopService.createCoffeShops({
            is_comentable: true,
            name: "cafeteria_cometable"
        });
        assert.notEqual(coffee_shop, null, "Error in create a comment");
    });

    it("Comment a comentable coffee shop with normal user", async()=>{
        let user = await initDataBaseService.getNormalUser();
        let coffee_shop = await initDataBaseService.getCommentableCoffeeShop();

        return await coffeeShopService.addNewComment(coffee_shop._id, user._id, {
            comment: "Nuevo cometario"
        }).then((coffeee_shop)=>{
            assert.notEqual(coffeee_shop, null, "Error in create a comment with normal user");
        });
    });

    it("Comment a uncomentable coffee shop with normal user", async()=>{
        let user = await initDataBaseService.getNormalUser();
        let coffee_shop = await initDataBaseService.getUnommentableCoffeeShop();

        return await coffeeShopService.addNewComment(coffee_shop._id, user._id, {
            comment: "Nuevo cometario"
        }).then(()=>{
            return Promise.reject();
        }).catch((error)=>{
            assert.notEqual(error, null, "Error in create a fail comment");
        });
    });

    it("Comment a uncomentable coffee shop with admin user", async()=>{
        let user = await initDataBaseService.getAdminUser();
        let coffee_shop = await initDataBaseService.getUnommentableCoffeeShop();

        return await coffeeShopService.addNewComment(coffee_shop._id, user._id, {
            comment: "Nuevo cometario"
        }).then((coffeee_shop)=>{
            assert.notEqual(coffeee_shop, null, "Error in create a comment with admin user");
        });
    });

});
