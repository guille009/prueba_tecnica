let assert = require('chai').assert;
let utils = require('./utils');
let mongoose = require('mongoose');
let UserService = require('../services/userService');
let InitDataBaseService = require('../services/initDataBase/initDataBaseService');
let userService = new UserService();
let initDataBaseService = new InitDataBaseService();
let client_api = require('./client_api/');

describe("Test de los usuarios", function() {

    before(async() => {
        if (mongoose.connection.db) return Promise.resolve();
        return await mongoose.connect(utils.db_uri_test);
    });

    before(async() => {
        return await initDataBaseService.deleteBBDD();
    });

    before(async() => {
        await initDataBaseService.initialData();
    });

    it("Add user", async()=>{
        let response = await
            client_api.addNewUser("usuario_prueba", "contra_prueba", false);
        assert.equal(response.status, 201, "Error to add user");
        return Promise.resolve();
    });

    it("Login", async()=>{
        let user = InitDataBaseService.prototype.noAdminUserSample.user;
        let password = InitDataBaseService.prototype.noAdminUserSample.password;
        let response = await client_api.login(user, password);
        let cookie_session = client_api.getSessionCookieFromResponse(response);
        assert.equal(response.status, 200, "Error in login");
        assert.notEqual(cookie_session, null, "Error in the cookie");
        return Promise.resolve();
    });

    it("Add duplicate user", async()=>{
        return userService.createNewUser({
            user: "usuario_prueba",
            password: "contra_prueba"
        }).catch((error)=>{
            assert.equal(error.error_code, 400, "Error in a duplicate user");
        });
    });

    it("Check session", async()=>{
        let user = await userService.checkLogin({
            user: "usuario_prueba",
            password: "contra_prueba"
        });
        assert.equal(user.user, "usuario_prueba", "Error in login");
    });

    it("Check fail session", async()=>{
        let result = await userService.checkLogin({
            user: "usuario_prueba_incorrecto",
            password: "contra_prueba"
        }).catch((error) => {return error;});
        assert.equal(result.error_code, 400, "Error in fail login");
    })


});