let axios = require('axios');
let util = require('../utils');

exports.addNewUser = async (user, password, is_admin)=>{
    return await axios.post(util.api_base_url+"/users",{
        user: user,
        password: password,
        is_admin: is_admin
    });
};

exports.login = async (user, password)=>{
    return await axios.post(util.api_base_url+"/users/login",{
        user: user,
        password: password,
    });
};

exports.getSessionCookieFromResponse = (response) => {
    return response.headers["set-cookie"][0];
};
