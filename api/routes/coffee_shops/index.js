let express = require('express');
let router = express.Router();
let coffeeShopsControllers = require('../../controllers/coffeShopsControllers');
let auth_control = require('../auth_control/index');


router.get('/', auth_control.userAuthenticateControl, coffeeShopsControllers.getCoffeeShops);
router.post('/', auth_control.userAuthenticateControl, coffeeShopsControllers.addCoffeeShops);
router.get('/:id/comments', auth_control.userAuthenticateControl, coffeeShopsControllers.getCommentsByCoffeeShop);
router.post('/:id/comments', auth_control.userAuthenticateControl, coffeeShopsControllers.addNewComments);

module.exports = router;