let auth = require('../../auth/');

exports.userAuthenticateControl = (req, res, next) => {
    if(!auth.isUserAuthenticate(req))
        return res.status(403).json({msg: "Access denied"});
    next();
};