let express = require('express');
let router = express.Router();
let usersControllers = require('../../controllers/usersControllers');

router.post('/login', usersControllers.login);
router.post('/logout', usersControllers.logout);
router.post('/', usersControllers.addUser);

module.exports = router;