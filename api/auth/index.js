let SessionService = require('../../services/sessionsService');
let sessionService = new SessionService();

exports.isUserAuthenticate = (req) => {
    return sessionService.isSessionInit(req, ["id_user"]);
};

exports.createSession = (req, user) => {
    sessionService.createSession(req, {id_user: user._id});
    return {}; // Cookie session is include in the response
};

exports.destroySession = (req) => {
    sessionService.destroySession(req);
};

exports.getSessionData = (req) => {
    return sessionService.getSessionData(req);
};

