let CoffeeShops = require('../../services/coffeShopService');
let coffeeShopsService = new CoffeeShops();

let auth = require('../auth/');

exports.getCoffeeShops = async (req, res, next) =>{
    coffeeShopsService.getAllWithoutComments()
    .then((coffee_shops)=>{
        res.status(200).json(coffee_shops);
    })
    .catch((error)=>{
        res.status(500).json({msg: "An error was happend"});
    });
};

exports.addCoffeeShops = async (req, res, next) =>{
    coffeeShopsService.createCoffeShops(req.body)
    .then((coffee_shop)=>{
        res.status(201).json(coffee_shop);
    })
    .catch((error)=>{
        res.status(500).json({msg: "An error was happend"});
    });
};

exports.getCommentsByCoffeeShop = async (req, res, next) =>{
    coffeeShopsService.getCommentsByCoffeeShop(req.params.id)
    .then((comments)=>{
        res.status(200).json(comments);
    })
    .catch((error)=>{
        res.status(500).json({msg: "An error was happend"});
    });
};

exports.addNewComments = async (req, res, next) =>{
    let session_data = auth.getSessionData(req);
    coffeeShopsService.addNewComment(req.params.id, session_data.id_user, {
        id_user: session_data.id_user,
        comment: req.body.comment
    }).then((coffee_shop)=>{
        res.status(201).json(coffeeShopsService.getLastCommentByCoffeeShop(coffee_shop));
    })
    .catch((error)=>{
        console.log(error);
        if(error && error.error_code){
            return res.status(error.error_code).json({msg: error.msg});
        }
        res.status(500).json({msg: "An error was happend"});
    });
};
