let UserService = require('../../services/userService');
let auth = require('../auth/');
let userService = new UserService();

exports.login = async (req, res, next) => {
    userService.checkLogin(req.body)
    .then((user)=>{
        let data_usar_session = auth.createSession(req, user);
        data_usar_session.msg = "Session create";
        res.status(200).json(data_usar_session);
    })
    .catch((error)=>{
        res.status(error.error_code).json({msg: error.msg});
    });
};

exports.logout = async (req, res, next) => {
    auth.destroySession(req);
    res.status(200).json({msg: "Session destroy"});
};

exports.addUser = async (req, res, next) => {
    userService.createNewUser(req.body)
    .then((user)=>{
        res.status(201).json({msg: "User create"});
    })
    .catch((error)=>{
        res.status(error.error_code).json({msg: error.msg});
    });
};